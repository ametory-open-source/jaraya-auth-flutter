class TokenModel {
  String? token;
  String? userToken;

  TokenModel({this.token, this.userToken});

  TokenModel.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    userToken = json['user_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['token'] = token;
    data['user_token'] = userToken;
    return data;
  }
}
