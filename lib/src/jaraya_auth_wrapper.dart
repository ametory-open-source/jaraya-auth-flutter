import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:jaraya_auth/src/models/token.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

class JarayaAuthWrapper extends StatefulWidget {
  final String title;
  final List<Widget>? actions;
  final String appId;
  final String scope;
  const JarayaAuthWrapper(
      {super.key,
      required this.title,
      this.actions,
      required this.appId,
      required this.scope});

  @override
  State<JarayaAuthWrapper> createState() => _JarayaAuthWrapperState();
}

class _JarayaAuthWrapperState extends State<JarayaAuthWrapper> {
  ScrollController _scrollcontroller = ScrollController();
  late final WebViewController _controller;
  late String existUrl;
  int progress = 100;

  @override
  void initState() {
    super.initState();
    existUrl =
        "http://app.jaraya.id/sso/login?app_id=${widget.appId}&redirect=0&scope=${widget.scope}";

    // #docregion platform_features
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
        WebViewController.fromPlatformCreationParams(params);
    // #enddocregion platform_features

    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int prg) {
            setState(() {
              progress = prg;
            });
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) async {
            debugPrint('Page finished loading: $url');
            try {
              var javascript = '''
      window.confirm = function (e){
        Confirm.postMessage(e);
      }
    ''';
              await _controller.runJavaScript(javascript);
            } catch (_) {}
          },
          onWebResourceError: (WebResourceError error) {
            debugPrint('''
Page resource error:
  code: ${error.errorCode}
  description: ${error.description}
  errorType: ${error.errorType}
  isForMainFrame: ${error.isForMainFrame}
          ''');
          },
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              debugPrint('blocking navigation to ${request.url}');
              return NavigationDecision.prevent;
            }
            debugPrint('allowing navigation to ${request.url}');
            return NavigationDecision.navigate;
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
        ),
      )
      ..addJavaScriptChannel(
        'JARAYACHANNEL',
        onMessageReceived: (JavaScriptMessage message) {
          Navigator.of(context)
              .pop(TokenModel.fromJson(jsonDecode(message.message)));
        },
      )
      ..loadRequest(Uri.parse(existUrl));

    // #docregion platform_features
    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }
    // #enddocregion platform_features

    _controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: SafeArea(
            child: WebViewWidget(
          controller: _controller,
        )));
  }
}
